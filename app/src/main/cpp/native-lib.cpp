#include <jni.h>
#include <string>

extern "C" JNIEXPORT jstring JNICALL
Java_com_android_brave_MainActivity_parseCurrencyFromJNI(
        JNIEnv* env,
        jobject instance,
        jobject loggableEntity_
        ) {
    char returnString[150];
    jclass loggableEntity = (*env).GetObjectClass(loggableEntity_);

    jfieldID timestampString = (*env).GetFieldID(loggableEntity, "timestamp", "Ljava/lang/String;");
    auto tString = (jstring) env->GetObjectField(loggableEntity_, timestampString);

    jfieldID priceString = (*env).GetFieldID(loggableEntity, "price", "Ljava/lang/String;");
    auto pString = (jstring) env->GetObjectField(loggableEntity_, priceString);

    jfieldID symbolString = (*env).GetFieldID(loggableEntity, "symbol", "Ljava/lang/String;");
    auto sString = (jstring) env->GetObjectField(loggableEntity_, symbolString);

    const char *nativeTimestamp = env->GetStringUTFChars(tString, nullptr);
    const char *nativePrice = env->GetStringUTFChars(pString, nullptr);
    const char *nativeSymbol = env->GetStringUTFChars(sString, nullptr);

    snprintf(returnString, sizeof returnString, "<%s> <%s> <%s>", nativeTimestamp, nativeSymbol, nativePrice);

    env->ReleaseStringUTFChars(tString, nativeTimestamp);
    env->ReleaseStringUTFChars(sString, nativeSymbol);
    env->ReleaseStringUTFChars(pString, nativePrice);

    return env->NewStringUTF(returnString);
}