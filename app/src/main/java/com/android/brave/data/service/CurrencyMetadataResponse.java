package com.android.brave.data.service;

import com.android.brave.data.entity.CurrencyMetadata;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Map;

public class CurrencyMetadataResponse {
    @SerializedName("data")
    @Expose
    private Map<String, CurrencyMetadata> data;

    public Map<String, CurrencyMetadata> getData() {
        return data;
    }

    public void setData(Map<String, CurrencyMetadata> data) {
        this.data = data;
    }
}
