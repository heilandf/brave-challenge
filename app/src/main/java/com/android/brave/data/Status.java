package com.android.brave.data;

public enum Status {
    SUCCESS,
    ERROR,
    LOADING
}
