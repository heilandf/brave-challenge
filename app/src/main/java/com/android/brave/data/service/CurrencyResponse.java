package com.android.brave.data.service;

import com.android.brave.data.entity.CurrencyEntity;

import java.util.Map;

public class CurrencyResponse {

    private Map<String, CurrencyEntity> data;

    public Map<String, CurrencyEntity> getResults() {
        return data;
    }

    public void setResults(Map<String, CurrencyEntity> results) {
        this.data = results;
    }
}
