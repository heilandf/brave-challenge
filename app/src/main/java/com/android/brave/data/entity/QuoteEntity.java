package com.android.brave.data.entity;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.Entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class QuoteEntity implements Parcelable {

    @SerializedName("price")
    @Expose
    private String price;

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    protected QuoteEntity(Parcel in) {
        price = in.readString();
    }

    public static final Creator<QuoteEntity> CREATOR = new Creator<QuoteEntity>() {
        @Override
        public QuoteEntity createFromParcel(Parcel in) {
            return new QuoteEntity(in);
        }

        @Override
        public QuoteEntity[] newArray(int size) {
            return new QuoteEntity[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.price);
    }
}
