package com.android.brave.data

import androidx.room.TypeConverter
import com.android.brave.data.entity.CurrencyQuoteEntity
import com.google.gson.Gson

class CurrencyQuoteConverter {
    @TypeConverter
    fun fromSource(source: CurrencyQuoteEntity): String {
        return source.toGson()
    }

    @TypeConverter
    fun toSource(value: String): CurrencyQuoteEntity {
        return value.fromGson(CurrencyQuoteEntity::class.java)
    }
}

fun <T> String.fromGson(classOfT: Class<T>): T = Gson().fromJson(this, classOfT)

fun Any.toGson(): String = Gson().toJson(this)