package com.android.brave.data.entity;

import com.google.gson.annotations.SerializedName;

public class CurrencyMetadata {
    @SerializedName("logo")
    private String logo;

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }
}
