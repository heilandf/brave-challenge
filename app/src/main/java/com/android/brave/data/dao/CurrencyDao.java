package com.android.brave.data.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.android.brave.data.entity.CurrencyEntity;

import java.util.List;

@Dao
public interface CurrencyDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertCurrency(CurrencyEntity currencyEntity);

    @Query("SELECT * FROM `CurrencyEntity` WHERE entityId = :id")
    CurrencyEntity getCurrencyById(String id);

    @Query("SELECT * FROM `CurrencyEntity`")
    List<CurrencyEntity> getCurrencies();
}
