package com.android.brave.data.service;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface CurrencyApiService {

    @GET("v1/cryptocurrency/quotes/latest?convert=USD,BTC&symbol=BTC,ETH,BNB,BAT")
    Observable<CurrencyResponse> fetchCurrencies();

    @GET("v2/cryptocurrency/info")
    Observable<CurrencyMetadataResponse> getCurrencyMetadata(@Query("id") String id);
}
