package com.android.brave.data.remote;

import com.example.brave.BuildConfig;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class RequestInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request.Builder requestBuilder = chain.request().newBuilder();
        requestBuilder.header("Content-Type", "application/json");
        requestBuilder.header("X-CMC_PRO_API_KEY", BuildConfig.API_KEY);
        return chain.proceed(requestBuilder.build());
    }
}

