package com.android.brave.data.entity;

public class LoggableEntity {
    private final String timestamp;
    private final String price;
    private final String symbol;

    public LoggableEntity(String timestamp, String symbol, String price) {
        this.timestamp = timestamp;
        this.price = price;
        this.symbol = symbol;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getPrice() {
        return price;
    }

    public String getSymbol() {
        return symbol;
    }
}
