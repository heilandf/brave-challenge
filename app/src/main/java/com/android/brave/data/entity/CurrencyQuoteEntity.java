package com.android.brave.data.entity;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.Entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CurrencyQuoteEntity implements Parcelable {

    @SerializedName("USD")
    @Expose
    private QuoteEntity usd;

    @SerializedName("BTC")
    @Expose
    private QuoteEntity btc;

    public QuoteEntity getUsd() {
        return usd;
    }

    public void setUsd(QuoteEntity usd) {
        this.usd = usd;
    }

    public QuoteEntity getBtc() {
        return btc;
    }

    public void setBtc(QuoteEntity btc) {
        this.btc = btc;
    }

    protected CurrencyQuoteEntity(Parcel in) {
        usd = in.readParcelable(QuoteEntity.class.getClassLoader());
        btc = in.readParcelable(QuoteEntity.class.getClassLoader());
    }

    public CurrencyQuoteEntity() {
        // Empty constructor
    }

    public static final Creator<CurrencyQuoteEntity> CREATOR = new Creator<CurrencyQuoteEntity>() {
        @Override
        public CurrencyQuoteEntity createFromParcel(Parcel in) {
            return new CurrencyQuoteEntity(in);
        }

        @Override
        public CurrencyQuoteEntity[] newArray(int size) {
            return new CurrencyQuoteEntity[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.usd, 0);
        parcel.writeParcelable(this.btc, 0);
    }
}
