package com.android.brave.data.entity;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.Entity;
import androidx.room.TypeConverters;

import com.android.brave.data.CurrencyQuoteConverter;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(primaryKeys = ("entityId"))
@TypeConverters(CurrencyQuoteConverter.class)
public class CurrencyEntity implements Parcelable {

    @Expose
    @SerializedName("id")
    private Long entityId;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("symbol")
    @Expose
    private String symbol;

    @SerializedName("image")
    @Expose
    private String image;

    @SerializedName("quote")
    @Expose
    private CurrencyQuoteEntity currencyQuote;

    @SerializedName("last_updated")
    @Expose
    private String lastUpdated;

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long id) {
        this.entityId = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public CurrencyQuoteEntity getCurrencyQuote() {
        return currencyQuote;
    }

    public void setCurrencyQuote(CurrencyQuoteEntity currencyQuote) {
        this.currencyQuote = currencyQuote;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.entityId);
        dest.writeString(this.name);
        dest.writeString(this.symbol);
        dest.writeString(this.image);
        dest.writeParcelable(this.currencyQuote, 0);
        dest.writeValue(this.lastUpdated);
    }

    public CurrencyEntity() {
        /* Empty Constructor */
    }

    protected CurrencyEntity(Parcel in) {
        this.entityId = (Long) in.readValue(Long.class.getClassLoader());
        this.name = in.readString();
        this.symbol = in.readString();
        this.image = in.readString();
        this.currencyQuote = in.readParcelable(CurrencyQuoteEntity.class.getClassLoader());
        this.lastUpdated = in.readString();
    }

    public static final Creator<CurrencyEntity> CREATOR = new Creator<CurrencyEntity>() {
        @Override
        public CurrencyEntity createFromParcel(Parcel source) {
            return new CurrencyEntity(source);
        }

        @Override
        public CurrencyEntity[] newArray(int size) {
            return new CurrencyEntity[size];
        }
    };

}
