package com.android.brave.data.repository;

import android.util.Log;

import androidx.annotation.NonNull;

import com.android.brave.data.NetworkBoundResource;
import com.android.brave.data.Resource;
import com.android.brave.data.dao.CurrencyDao;
import com.android.brave.data.entity.CurrencyEntity;
import com.android.brave.data.entity.CurrencyMetadata;
import com.android.brave.data.service.CurrencyApiService;
import com.android.brave.data.service.CurrencyMetadataResponse;
import com.android.brave.data.service.CurrencyResponse;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Singleton;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Function;

@Singleton
public class CurrencyRepository {
    private final CurrencyDao currencyDao;
    private final CurrencyApiService currencyService;
    public CurrencyRepository(CurrencyDao currencyDao,
                           CurrencyApiService apiService) {
        this.currencyDao = currencyDao;
        this.currencyService = apiService;
    }

    /*
     * Three methods called:
     * a. fetch data from server
     * b. fetch data from local
     * c. save data from api in local
     * */
    public Observable<Resource<List<CurrencyEntity>>> loadCurrencies() {
        return new NetworkBoundResource<List<CurrencyEntity>, CurrencyResponse>() {

            @Override
            protected void saveCallResult(@NonNull CurrencyResponse items) {
                for (CurrencyEntity value : items.getResults().values()) {
                    currencyDao.insertCurrency(value);
                }
            }

            @Override
            protected boolean shouldFetch() {
                return true;
            }

            @NonNull
            @Override
            protected Flowable<List<CurrencyEntity>> loadFromDb() {
                List<CurrencyEntity> entities = currencyDao.getCurrencies();
                if(entities == null || entities.isEmpty()) {
                    return Flowable.empty();
                }
                return Flowable.just(entities);
            }

            @Override
            protected void onFetchFailed(Throwable t) {
                Log.i("Tag", "Fetch Failed" + t);
            }

            @NonNull
            @Override
            protected Observable<Resource<CurrencyResponse>> createCall() {
                return currencyService.fetchCurrencies()
                        /* iterable currencies  */
                        .flatMapIterable((Function<CurrencyResponse, Iterable<CurrencyEntity>>) currencyResponse -> currencyResponse.getResults().values())
                        /* for every currency fetch its logo hitting a different endpoint unless we already have the currency locally stored */
                        .flatMap(new Function<CurrencyEntity, ObservableSource<CurrencyMetadataResponse>>() {
                            @Override
                            public ObservableSource<CurrencyMetadataResponse> apply(CurrencyEntity o) throws Exception {
                                String id = o.getEntityId().toString();
                                CurrencyEntity local = currencyDao.getCurrencyById(id);
                                if (local == null) {
                                    return currencyService.getCurrencyMetadata(id);
                                } else {
                                    Map<String, CurrencyMetadata> map = new HashMap<>();
                                    CurrencyMetadata metadata = new CurrencyMetadata();
                                    CurrencyMetadataResponse response = new CurrencyMetadataResponse();
                                    metadata.setLogo(local.getImage());
                                    map.put(id, metadata);
                                    response.setData(map);
                                    return Observable.just(response);
                                }
                            }
                        /* BiFunction to merge the metadata of the currency within the currency itself */
                        }, new BiFunction<CurrencyEntity, CurrencyMetadataResponse, CurrencyEntity>() {
                            @Override
                            public CurrencyEntity apply(CurrencyEntity currencyEntity, CurrencyMetadataResponse currencyMetadataResponse) throws Exception {
                                CurrencyMetadata metadata = currencyMetadataResponse.getData().get(currencyEntity.getEntityId().toString());
                                if (metadata != null) {
                                    currencyEntity.setImage(metadata.getLogo());
                                }
                                return currencyEntity;
                            }
                        })
                        .toList()
                        /* Map the entities list back to a CurrencyResponse object*/
                        .map(new Function<List<CurrencyEntity>, CurrencyResponse>() {
                            @Override
                            public CurrencyResponse apply(List<CurrencyEntity> currencyEntities) throws Exception {
                                CurrencyResponse response = new CurrencyResponse();
                                Map<String, CurrencyEntity> map = new HashMap<>();
                                for (CurrencyEntity e : currencyEntities) {
                                    map.put(e.getEntityId().toString(), e);
                                }

                                response.setResults(map);

                                return response;
                            }
                        })
                        .toObservable()
                        .flatMap(apiResponse -> Observable.just(apiResponse == null
                                ? Resource.error("", new CurrencyResponse())
                                : Resource.success(apiResponse)));
            }
        }.getAsObservable();
    }
}
