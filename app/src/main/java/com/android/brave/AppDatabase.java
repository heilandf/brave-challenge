package com.android.brave;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.android.brave.data.dao.CurrencyDao;
import com.android.brave.data.entity.CurrencyEntity;

@Database(entities = {CurrencyEntity.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    public abstract CurrencyDao currencyDao();
}
