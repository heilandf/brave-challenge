package com.android.brave;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.brave.data.entity.CurrencyEntity;
import com.android.brave.data.entity.LoggableEntity;
import com.android.brave.ui.CurrencyAdapter;
import com.android.brave.ui.CurrencyViewModel;
import com.example.brave.R;
import com.example.brave.databinding.ActivityMainBinding;

import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.inject.Inject;

import dagger.android.AndroidInjection;

public class MainActivity extends AppCompatActivity {

    static {
        System.loadLibrary("brave");
    }

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    private CurrencyAdapter adapter;
    private CurrencyViewModel viewModel;

    private final Handler handler = new Handler();
    private Runnable runnable;
    // 5 seconds constant for currencies updates
    private static final int delay = 5 * 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        ActivityMainBinding binding = ActivityMainBinding.inflate(getLayoutInflater());
        viewModel = new ViewModelProvider(this, viewModelFactory).get(CurrencyViewModel.class);

        setContentView(binding.getRoot());

        initAdapter();

        observeViewModel(viewModel);

        // initial load
        viewModel.loadCurrencies();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        // to-do: add search functionality
        return true;
    }

    private void observeViewModel(CurrencyViewModel viewModel) {
        viewModel.getCurrencies().observe(this, listResource -> {
            if (listResource.isSuccess()) {
                Log.i(MainActivity.class.getCanonicalName(), "New data loaded");
                adapter.setData(listResource.data);
                assert listResource.data != null;
                for (CurrencyEntity e : listResource.data) {
                    SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                    LoggableEntity le = new LoggableEntity(
                            s.format(new Date()),
                            e.getSymbol(),
                            e.getCurrencyQuote().getUsd().getPrice()
                    );

                    // Appending timestamp + symbol + price in native code
                    appendLog(parseCurrencyFromJNI(le));
                }
            } else if (listResource.isRecoverableError()) {
                // Recoverable error since we have locally stored data
                Log.e(MainActivity.class.getCanonicalName(), listResource.message);
                adapter.setData(listResource.data);
            } else if (listResource.isLoading() && listResource.data != null) {
                Log.i(MainActivity.class.getCanonicalName(), "New data is loading");
                adapter.setData(listResource.data);
            }
        });
    }

    private void initAdapter() {
        RecyclerView recyclerView = findViewById(R.id.rv_assets);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new CurrencyAdapter();
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        /* Check every 5 seconds for currencies update */
        handler.postDelayed( runnable = () -> {
            viewModel.loadCurrencies();
            handler.postDelayed(runnable, delay);
        }, delay);

        super.onResume();
    }

    @Override
    protected void onPause() {
        handler.removeCallbacks(runnable);
        super.onPause();
    }

    public void appendLog(String log) {
        try {
            String FILENAME = "log_file";

            FileOutputStream fos = openFileOutput(FILENAME, Context.MODE_APPEND);
            fos.write(log.getBytes());
            fos.write('\n');
            fos.close();

        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public native String parseCurrencyFromJNI(LoggableEntity loggableEntity);
}