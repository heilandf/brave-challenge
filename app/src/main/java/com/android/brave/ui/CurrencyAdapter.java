package com.android.brave.ui;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.brave.data.entity.CurrencyEntity;
import com.example.brave.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class CurrencyAdapter extends RecyclerView.Adapter<CurrencyAdapter.ViewHolder> {

    private List<CurrencyEntity> data;

    public CurrencyAdapter() {
        data = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.currency_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        CurrencyEntity currency = data.get(position);
        hideSecondaryPriceForBTC(holder, currency);
        Picasso.get().load(currency.getImage()).into(holder.getIvLogo());
        holder.getTvCurrencyTitle().setText(currency.getName());
        Double btcPrice = Double.valueOf(currency.getCurrencyQuote().getBtc().getPrice());
        Double usdPrice = Double.valueOf(currency.getCurrencyQuote().getUsd().getPrice());
        holder.getTvPriceBTC().setText(String.format(Locale.getDefault(), "%.5f", btcPrice));
        holder.getTvPriceUSD().setText(String.format(Locale.getDefault(), "%.5f", usdPrice));
    }

    private void hideSecondaryPriceForBTC(@NonNull ViewHolder holder, CurrencyEntity currency) {
        if (currency.getSymbol().equalsIgnoreCase("btc")) {
            holder.getTvPriceBTC().setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setData(List<CurrencyEntity> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView tvCurrencyTitle;
        private final TextView tvPriceUSD;
        private final TextView tvPriceBTC;
        private final ImageView ivLogo;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvCurrencyTitle = itemView.findViewById(R.id.tv_currency_title);
            ivLogo = itemView.findViewById(R.id.iv_logo);
            tvPriceBTC = itemView.findViewById(R.id.tv_currency_btc);
            tvPriceUSD = itemView.findViewById(R.id.tv_currency_usd);
        }

        public TextView getTvCurrencyTitle() {
            return tvCurrencyTitle;
        }

        public TextView getTvPriceUSD() {
            return tvPriceUSD;
        }

        public TextView getTvPriceBTC() {
            return tvPriceBTC;
        }

        public ImageView getIvLogo() {
            return ivLogo;
        }
    }

}
