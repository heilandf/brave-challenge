package com.android.brave.ui;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.android.brave.data.Resource;
import com.android.brave.data.dao.CurrencyDao;
import com.android.brave.data.entity.CurrencyEntity;
import com.android.brave.data.repository.CurrencyRepository;
import com.android.brave.data.service.CurrencyApiService;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class CurrencyViewModel extends ViewModel {

    private final CurrencyRepository currencyRepository;
    private final MutableLiveData<Resource<List<CurrencyEntity>>> currenciesLiveData = new MutableLiveData<>();
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Inject
    public CurrencyViewModel(CurrencyDao dao, CurrencyApiService service) {
        currencyRepository = new CurrencyRepository(dao, service);
    }

    public void loadCurrencies() {
        getCurrencies().postValue(Resource.loading(null));
        compositeDisposable.add(currencyRepository
                .loadCurrencies()
                .subscribe(resource -> getCurrencies().postValue(resource)));
    }

    @Override
    protected void onCleared() {
        if (!compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
        super.onCleared();
    }

    public MutableLiveData<Resource<List<CurrencyEntity>>> getCurrencies() {
        return currenciesLiveData;
    }
}
