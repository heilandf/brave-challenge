package com.android.brave.di;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.room.Room;

import com.android.brave.AppDatabase;
import com.android.brave.data.dao.CurrencyDao;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DbModule {
    @Provides
    @Singleton
    AppDatabase provideDatabase(@NonNull Application application) {
        return Room.databaseBuilder(application,
                AppDatabase.class, "Entertainment.db")
                .allowMainThreadQueries().build();
    }

    @Provides
    @Singleton
    CurrencyDao provideCurrencyDao(@NonNull AppDatabase appDatabase) {
        return appDatabase.currencyDao();
    }
}